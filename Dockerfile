# Utilisez l'image WordPress officielle en tant que base
FROM wordpress:latest

# Copiez votre site WordPress dans le répertoire web du conteneur
COPY ./ /var/www/html/

# Exposez le port 80
EXPOSE 80

CMD ["apache2-foreground"]