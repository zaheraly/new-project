<?php

get_header();
pageBanner(array(
  'title' => 'Our Campuses',
  'subtitle' => 'We have several conveniently located campuses.'
));
 ?>

<div class="container container--narrow page-section">

<!-- <div class="acf-map"> -->
<ul class="link-list min-list">

<?php
  while(have_posts()) {
    the_post();
    $mapLocation = get_field('map_location');
    // echo do_shortcode('[leaflet-map lat=59.913 lng=10.739 zoom=12][leaflet-marker]OSLO![/leaflet-marker]');
   ?>
   <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
  <?php }
  echo paginate_links();
?>
</ul>
<!-- </div> -->



</div>

<?php get_footer();

?>